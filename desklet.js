imports.searchPath.push( imports.ui.deskletManager.deskletMeta["toggl@Karolis"].path );

const Gio = imports.gi.Gio;
const St = imports.gi.St;
const Desklet = imports.ui.desklet;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Clutter = imports.gi.Clutter;
const GLib = imports.gi.GLib;
const Tweener = imports.ui.tweener;
const Util = imports.misc.util;
const Main = imports.ui.main;
const Tooltips = imports.ui.tooltips;
const PopupMenu = imports.ui.popupMenu;
const Cinnamon = imports.gi.Cinnamon;
const Settings = imports.ui.settings;
const TogglReader=imports.togglreader;

const Gettext = imports.gettext.domain('cinnamon-applets');
const _ = Gettext.gettext;

const Applet = imports.ui.applet;

const Soup = imports.gi.Soup;

function MyDesklet(metadata,decklet_id){
    this._init(metadata,decklet_id);
}

MyDesklet.prototype = {
    __proto__: Desklet.Desklet.prototype,


    _init: function(metadata,decklet_id){
        this.proces=null;

        var this_=this;
        var reader=null;
        try {
            Desklet.Desklet.prototype._init.call(this, metadata);
            this.settings = new Settings.DeskletSettings(this, "toggl@Karolis", this.desklet_id);
            this.settings.bindProperty(Settings.BindingDirection.ONE_WAY,"zoom","zoom",this._refreshtask,null);
            this.settings.bindProperty(Settings.BindingDirection.ONE_WAY,"apiid","apiid",this._refreshtask,null);
            //this.tasks.style = 'text-align : right;font-size: '+14*this.zoom+"px";
            this.proces=true;
            this.reader = new TogglReader.TogglReader({
                'username':'asdsd',
                'password':'sdasdsdsd',
                'callbacks':{
                    'onError':function(a_code,a_params){this_.onError(a_code,a_params)},
                    'onNewMail':function(a_params){this_.onGfNewMail(a_params)},
                    'onNoNewMail':function(a_params){this_.onGfNoNewMail()}
                }
            });
            this._refreshtask();
            this.humidity.text =  imports.ui.deskletManager.deskletMeta["toggl@Karolis"].path;
        }
        catch (e) {
            global.logError(e);
        }
        return true;
    },

//##########################REFRESH#########################
    createwindow: function(){

        this.window=new St.BoxLayout({vertical: false});
        this.cweather = new St.BoxLayout({vertical: true});
        this.container= new St.BoxLayout({vertical: true, x_align: St.Align.MIDDLE});
        this.ctemp = new St.BoxLayout({vertical: false,x_align: 2});

        this.humidity=new St.Label();

        this.ctemp_values = new St.BoxLayout({vertical: true, style : 'text-align : left; font-size: '+14*this.zoom+"px"});
        this.ctemp_values = new St.BoxLayout({vertical: true, style : 'text-align : left; font-size: '+14*this.zoom+"px"});


        this.ctemp_values.add_actor(this.humidity);

        this.cwicon = new St.Bin({height: (500*this.zoom), width: (200*this.zoom)});
        this.cweather.add_actor(this.cwicon);

        this.tasks = new St.BoxLayout({vertical: true,style : 'text-align : right'});
        this.tasks.add_actor(new St.Label({text: _('Message: ')}));

        this.ctemp.add_actor(this.ctemp_values);
        this.ctemp.add_actor(this.tasks);
        this.container.add_actor(this.ctemp);

        this.window.add_actor(this.cweather);
        this.window.add_actor(this.container);
    },

    _refreshtask: function() {
        if (this.proces)
        {
            this.createwindow();
            this.setContent(this.window);
            this.humidity.text = 'ttttt';
            this.humidity.text =  imports.ui.deskletManager.deskletMeta["toggl@Karolis"].path;
            this.reader.check();
            this.humidity.text = 'chekced';
            //this._timeoutId=Mainloop.timeout_add_seconds(120+ Math.round(Math.random()*120), Lang.bind(this, this._refreshtask()));
        }
    },

    _onError: function(a_code,a_params) {
        this.humidity.text = a_code;
    }
}

function main(metadata, decklet_id){
    let desklet = new MyDesklet(metadata,decklet_id);
    return desklet;
}